// JavaScript Document

// setup
var c = document.createElement('canvas');
var ctx = c.getContext('2d');
var W, H;
document.body.appendChild(c);

// wow, very responsive, such resize
function resize(){
  // dim
  W = c.width = window.innerWidth;
  H = c.height = window.innerHeight;

  // bg
  //ctx.fillStyle = '#222';
  ctx.fillRect(0, 0, W, H);
}
resize();

function clearDrawSpace(){
W = c.width = window.innerWidth;
  H = c.height = window.innerHeight;

  // bg
  //ctx.fillStyle = '#222';
  ctx.fillRect(0, 0, W, H);
	
	displayText();
}


// display the fucking text
function displayText(){
  // boring	
	var quote = document.getElementById("userInput1").value;
	var size = document.getElementById("userInput2").value;
	var fontType = document.getElementById("userInput3").value;
	
  ctx.fillStyle = '#fff';
  ctx.textBaseline = 'top';
  
  ctx.font = size+'px '+fontType;
  ctx.fillText(quote, W/2 - 180, H/2 - 140); // txt, offsetX, offsetY
  
  //ctx.font = 'bold 140px Open Sans';
  //ctx.fillText('Diem', W/2 - 50, H/2);
}



// resize handler
window.addEventListener('resize', function(){ 
  resize();
  displayText();
}, false);