var textCanvas = (function(){

	/* Internal variables */
	
	var canvas;
	
	var backGround;
	
	var loadedImage;
	
	var citat1;
	
	var citat2;
	
	var citat3;
	
	var citat4;
	
	function init(areaid, width, height, backGroundImage){

		// first load the html ui
		$( "#"+areaid ).load( "lib/main.html", function(){

			//Creating the Canvas object
			canvas = new fabric.Canvas('totallyuniquecanvasname');

			//creates the background object and places it on the canvas
			backGround = new fabric.Rect({
				left: 0,
				top: 0,
				fill: 'transparent',
				width: width,
				height: height,
				selectable: false,
			});
			canvas.add(backGround);
			
			//loads in an image
			loadedImage = new fabric.Image(backGroundImage, {
				left: 0,
				top: 0,
				width: width,
				height: height,
				selectable:false
			});
			canvas.add(loadedImage);

			//Creating the text objects and adding them to the canvas
			citat1 = new fabric.Text('Something is not working', { left: 50, top: 100, quoteNr: 1 });
			citat2 = new fabric.Text('', { left: 50, top: 100, selectable: false, quoteNr: 2 });
			citat3 = new fabric.Text('', { left: 50, top: 100, selectable: false, quoteNr: 3 });
			citat4 = new fabric.Text('', { left: 50, top: 100, selectable: false, quoteNr: 4 });
			canvas.add(citat1, citat2, citat3, citat4);
			
			canvas.setHeight(height);
			canvas.setWidth(width);
			
			//listener for object selection
			canvas.on('mouse:down', updateSelection);

		});

	}

	/* internal functions */
	
	//Updates the values in the form to match the selected object
	function updateSelection(){
		if(canvas.getActiveObject()!==undefined && canvas.getActiveObject()!==null && canvas.getActiveObject().type === 'text'){
			document.getElementById("userInputCitat").value = canvas.getActiveObject().quoteNr;
			document.getElementById("userInput1").value = canvas.getActiveObject().text;
			document.getElementById("userInput2").value = canvas.getActiveObject().fontSize;
			document.getElementById("userInput3").value = canvas.getActiveObject().fontFamily;
			document.getElementById("userInput5").value = canvas.getActiveObject().fill;
		}	
	}
	
	//Rendering function that is called every time the canvas needs to be re-drawn
	function render(){
		canvas.renderAll();
		console.log(canvas.getObjects());
	}

	//Function for a button that allows you to apply all your setting on the canvas
	function generate(){
		var quote = document.getElementById("userInput1").value;
		var size = document.getElementById("userInput2").value;
		var fontType = document.getElementById("userInput3").value;
		var quoteNumber = document.getElementById("userInputCitat").value;
		var backGroundColor = document.getElementById("userInput4").value;
		var textColor = document.getElementById("userInput5").value;

		if(quoteNumber === "1"){canvas.item(2).set('text', quote).set({fontFamily: fontType, fontSize: size, fill:textColor});}

		else if(quoteNumber === "2"){canvas.item(3).set('text', quote).set({fontFamily: fontType, fontSize: size, selectable: true, fill:textColor});}

		else if(quoteNumber === "3"){canvas.item(4).set('text', quote).set({fontFamily: fontType, fontSize: size, selectable: true, fill:textColor});}

		else if(quoteNumber === "4"){canvas.item(5).set('text', quote).set({fontFamily: fontType, fontSize: size, selectable: true, fill:textColor});}

		canvas.item(0).set({fill:backGroundColor});

		render();

	} 

	//Function that allows you to open an png image of the canvas
	function bild(){
		var image = canvas.toDataURL("image/png");
		var w = window.open("");
		w.document.write('<img src="'+image+'"/>');
	}

	//Function that resets the canvas
	function reset(){
		canvas.item(2).set('text', 'Enter text here...').set({fontFamily: 'Times New Roman', fontSize: 50 , fill: 'Black', left: 50, top: 100, angle: 0});

		canvas.item(3).set('text', '').set({selectable: false, left: 50, top: 100, angle: 0});

		canvas.item(4).set('text', '').set({selectable: false, left: 50, top: 100, angle: 0});

		canvas.item(5).set('text', '').set({selectable: false, left: 50, top: 100, angle: 0});

		canvas.item(0).set({fill:'Transparent'});
		render();
		
		//Updates the values of the form to match the imported image
		document.getElementById("userInputCitat").value = canvas.item(2).quoteNr;
		document.getElementById("userInput1").value = canvas.item(2).text;
		document.getElementById("userInput2").value = canvas.item(2).fontSize;
		document.getElementById("userInput3").value = canvas.item(2).fontFamily;
		document.getElementById("userInput5").value = canvas.item(2).fill;
		document.getElementById("userInput4").value = canvas.item(0).fill;
	}

	//Function that saves the canvas as a JSON
	function saveCanvas() {
		// convert canvas to a json string
		localStorage.setItem('jsonCanvas', JSON.stringify(canvas.toJSON(['selectable', 'quoteNr'])));
		console.log('Success');
	}

	//Function that loads the JSON previously saved
	function loadCanvas() {
		canvas.loadFromJSON(JSON.parse(localStorage.getItem('jsonCanvas'))) ;
		console.log(JSON.parse(localStorage.getItem('jsonCanvas')));
		// Makes sure the background proprerly fills the canvas
		canvas.item(0).set({width:canvas.getWidth(), height: canvas.getHeight()});
		// re-render the canvas
		render();
		
		//Updates the values of the form to match the imported image
		document.getElementById("userInputCitat").value = canvas.item(2).quoteNr;
		document.getElementById("userInput1").value = canvas.item(2).text;
		document.getElementById("userInput2").value = canvas.item(2).fontSize;
		document.getElementById("userInput3").value = canvas.item(2).fontFamily;
		document.getElementById("userInput5").value = canvas.item(2).fill;
		document.getElementById("userInput4").value = canvas.item(0).fill;
	}

	
	//return function for the internal functions
	return { 
		generate: generate,
		image: bild,
		reset: reset,
		save: saveCanvas,
		load: loadCanvas,
		init: init
	};

})();



